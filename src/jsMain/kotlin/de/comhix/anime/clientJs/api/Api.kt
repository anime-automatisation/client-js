package de.comhix.anime.clientJs.api

import de.comhix.anime.sharedCode.api.*
import de.comhix.anime.sharedCode.data.*
import de.comhix.anime.sharedCode.data.user.*
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.auth.Auth
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.plugin
import io.ktor.client.request.*
import io.ktor.client.request.forms.submitForm
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.json
import kotlinx.browser.localStorage
import kotlinx.browser.window
import kotlinx.serialization.json.Json
import org.w3c.dom.get
import org.w3c.dom.set

/**
 * @author Benjamin Beeker
 */
object Api {
    private val client = HttpClient {
        install(Auth) {
            bearer {
                loadTokens {
                    localStorage["token"]?.let {
                        console.info("found token")
                        BearerTokens(it, it)
                    } ?: run {
                        console.info("no token in localStorage")
                        null
                    }
                }
            }
        }
        install(ContentNegotiation) {
            json(Json {
                serializersModule = SerializationModule
            })
        }
        defaultRequest {
            url(ApiBase.url)
            contentType(ContentType.Application.Json)
        }
    }

    fun clearToken() {
        localStorage.removeItem("token")
        client.plugin(Auth)
            .providers
            .filterIsInstance<BearerAuthProvider>()
            .first()
            .clearToken()
    }

    fun setToken(token: String) {
        clearToken()
        localStorage["token"] = token
    }

    object Users : UserApi {
        override suspend fun changePassword(request: PasswordChangeRequest) {
            client.post("user/change-password") {
                setBody(request)
            }
        }

        override suspend fun login(request: LoginRequest): LoginResponse {
            return client.post("user/login") {
                setBody(request)
            }.body()
        }

        override suspend fun register(request: RegisterRequest): RegisterResponse {
            return client.post("user/register") {
                setBody(request)
            }.body()
        }

        suspend fun logout() {
            client.post("user/logout")
            clearToken()
        }

        suspend fun getUser(): User {
            return client.get("user/userinfo").body()
        }
    }

    object Animes : AnimeApi {
        override suspend fun list(): List<Anime> {
            return client.get("anime").body()
        }

        override suspend fun update(anime: Anime) {
            client.post("anime") {
                setBody(anime)
            }
        }
    }

    object Episodes : EpisodeApi {
        override suspend fun list(): List<Episode> {
            return client.get("episode").body()
        }
    }

    object Files : FileApi {
        override suspend fun list(): List<File> {
            return client.get("file").body()
        }
    }

    object Status : StatusApi {
        override suspend fun loadDatabaseStatus(): DatabaseStatus {
            return client.get("status/database-status").body()
        }

        override suspend fun loadServerVersions(): List<Version> {
            return client.get("status/version").body()
        }

        suspend fun loadClientVersion(): Version {
            return client.get("version.json") {
                host = window.location.host
            }.body()
        }
    }

    object Torrents : TorrentApi {
        override suspend fun addTorrent(magnet: String) {
            client.submitForm("torrent", formParameters = Parameters.build {
                append("magnet-url", magnet)
            })
        }

        override suspend fun getTorrents(): List<Torrent> {
            return client.get("torrent").body()
        }
    }
}