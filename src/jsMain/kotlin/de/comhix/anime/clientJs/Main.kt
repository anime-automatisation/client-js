package de.comhix.anime.clientJs

import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

/**
 * @author Benjamin Beeker
 */

fun main() {
    val navigationContainer = document.getElementById("navigation") ?: error("Couldn't find container!")
    val titleContainer = document.getElementById("title") ?: error("Couldn't find container!")
    titleContainer.innerHTML = ""
    val contentContainer = document.getElementById("content") ?: error("Couldn't find container!")
    contentContainer.innerHTML = ""

    MainScope().launch {
        Client(navigationContainer, titleContainer, contentContainer).render()
    }
}