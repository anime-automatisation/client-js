package de.comhix.anime.clientJs.components.user

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.clientJs.components.Component
import de.comhix.anime.clientJs.components.addInput
import de.comhix.anime.sharedCode.data.user.LoginRequest
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.html.DIV
import kotlinx.html.InputType
import kotlinx.html.form
import kotlinx.html.js.onSubmitFunction
import kotlinx.html.submitInput

/**
 * @author Benjamin Beeker
 */
object Login : Component<Any?>() {
    override suspend fun prepareData(): Any? {
        return null
    }

    override fun DIV.render() {
        form {
            var usernameValue = ""
            var passwordValue = ""
            addInput("username", "Name") {
                usernameValue = it
            }
            addInput("password", "Password", type = InputType.password) {
                passwordValue = it
            }
            submitInput(classes = "centered")

            onSubmitFunction = {
                it.preventDefault()

                MainScope().launch {
                    val token = Api.Users.login(LoginRequest(usernameValue, passwordValue)).token

                    Api.setToken(token!!)
                    navigate("")
                }
            }
        }
    }

}