package de.comhix.anime.clientJs.components.notification.toast

import de.comhix.anime.clientJs.components.notification.toast.ToastType.Info

/**
 * @author Benjamin Beeker
 */
data class Toast(val text: String, val type: ToastType = Info, val title: String = type.name)

enum class ToastType {
    Info,
    Warning
}