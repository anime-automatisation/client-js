package de.comhix.anime.clientJs.components

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.sharedCode.data.DatabaseStatus
import de.comhix.anime.sharedCode.data.Version
import kotlinx.html.*
import de.comhix.anime.clientJs.info.Version as ClientJsVersion
import de.comhix.anime.sharedCode.info.Version as ClientSharedVersion

/**
 * @author Benjamin Beeker
 */
object Info : Component<InfoData>() {
    override suspend fun prepareData(): InfoData {
        val serverVersions = try {
            Api.Status.loadServerVersions()
                .map {
                    val system = when (it.system) {
                        "api" -> "Api Build"
                        "shared" -> "Api Shared Code Build"
                        else -> it.system
                    }

                    it.copy(system = system)
                }
        }
        catch (e: Exception) {
            listOf(Version("Api", "failed", ""))
        }
        val clientVersion = try {
            Api.Status.loadClientVersion()
        }
        catch (e: Exception) {
            Version("Client", "failed", "")
        }

        val versions: List<Version> = serverVersions +
                clientVersion +
                Version("Client JS Build", ClientJsVersion.BUILD_VERSION, ClientJsVersion.BUILD_DATE) +
                Version("Client Shared Code Build", ClientSharedVersion.BUILD_VERSION, ClientSharedVersion.BUILD_DATE)
        val databaseStatus = try {
            Api.Status.loadDatabaseStatus()
        }
        catch (e: Exception) {
            DatabaseStatus(false, "failed")
        }

        return InfoData(
            versions,
            databaseStatus
        )
    }

    override fun DIV.render() {
        table {
            thead {
                tr {
                    th {
                        text("System")
                    }
                    th {
                        text("Version")
                    }
                    th {
                        text("Buildtime")
                    }
                }
            }
            data.versions.forEach {
                tr {
                    td {
                        text(it.system)
                    }
                    td {
                        text(it.version)
                    }
                    td {
                        text(it.date)
                    }
                }
            }
            tr {
                td {
                    text("Database Status")
                }
                td {
                    text(data.databaseStatus.version)
                }
                td {
                    text(if (data.databaseStatus.connected) "✔" else "❌")
                }
            }
        }
    }
}

data class InfoData(
    val versions: List<Version>,
    val databaseStatus: DatabaseStatus
)

//Client build	2.3.226	Tue Jan 17 22:52:38 UTC 2023
//Client JS build	4.2.13	Tue Jan 17 22:44:09 UTC 2023
//Client Shared Code build	4.0.60	Sat Dec 31 13:22:22 GMT 2022
//Api build	7.5.13	Tue Jan 17 22:42:53 UTC 2023
//Api Shared Code build	4.0.13	Tue Jan 17 22:42:53 UTC 2023
//Database	6.0.3	✔