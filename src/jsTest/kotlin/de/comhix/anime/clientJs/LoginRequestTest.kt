package de.comhix.anime.clientJs

import de.comhix.anime.sharedCode.data.SerializationModule
import de.comhix.anime.sharedCode.data.user.LoginRequest
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * @author Benjamin Beeker
 */
class LoginRequestTest {
    private val json = Json { serializersModule = SerializationModule }

    @Test
    fun testSerializable() {
        // given
        val loginRequest = LoginRequest("user name", "pass word", true)

        // when
        val json = this.json.encodeToString(loginRequest)
        val decoded: LoginRequest = this.json.decodeFromString(json)

        // then
        assertEquals(loginRequest, decoded)
    }
}