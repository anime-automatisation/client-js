package de.comhix.anime.clientJs

import de.comhix.anime.clientJs.components.*
import de.comhix.anime.clientJs.components.animes.Animes
import de.comhix.anime.clientJs.components.animes.Episodes
import de.comhix.anime.clientJs.components.torrents.TorrentsMenu
import de.comhix.anime.clientJs.components.user.Login
import de.comhix.anime.clientJs.navigation.Navigation
import de.comhix.anime.clientJs.utils.getParameter
import de.comhix.anime.clientJs.utils.warn
import org.w3c.dom.Element
import org.w3c.dom.get

/**
 * @author Benjamin Beeker
 */
class Client(private val navigationContainer: Element, private val titleContainer: Element, private val contentContainer: Element) {

    private val pages: List<Component<*>> = listOf(Downloads, Login, Info, Animes, Episodes, TorrentsMenu)
    suspend fun render() {
        Navigation.create().let {
            for (i in 0..it.length) {
                it[i]?.let { node ->
                    navigationContainer.appendChild(node)
                }
            }
        }

        val component = getComponent()

        titleContainer.innerHTML = component.title

        contentContainer.append(component.render())
    }

    private fun getComponent(): Component<*> {
        val page = getParameter("page")

        return pages.find { it.path == page } ?: run {
            warn { "page $page not found" }
            Downloads
        }
    }
}