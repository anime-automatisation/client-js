import de.comhix.gradle.plugins.version.Version
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

project.group = "de.comhix.anime"
project.version = Version(6, 0)

val jsName = "${project.name}_${version.toString().replace(".", "-")}.js"

repositories {
    mavenCentral()
}

plugins {
    kotlin("multiplatform") version buildVersions.DependencyVersions.kotlinVersion
    kotlin("plugin.serialization") version buildVersions.DependencyVersions.kotlinVersion
    //    id("jacoco")
    id("maven-publish")
    //    id("signing")
    //    id("org.jetbrains.dokka") version "0.10.0"
    id("org.sonatype.gradle.plugins.scan") version buildVersions.PluginVersion.ossScan
    id("de.comhix.gradle.plugins.version") version buildVersions.PluginVersion.version
}

//jacoco.toolVersion = "0.8.7"

val generateSources: Task by tasks.creating {
    outputs.dir("${project.projectDir}/src/generated/kotlin")

    doFirst {
        generateFile(
            "${project.group}.${project.name}.info",
            "Version.kt",
            """object Version{
    const val BUILD_VERSION = "${project.version}"
    const val BUILD_DATE = "${Date()}"
    const val BUILD_TIMESTAMP = ${Date().time}
}
"""
        )

        val dollar = "\$"
        val apiUrl =
            if (System.getenv("CI_PIPELINE_IID").isNullOrBlank())
                "http://localhost:8080"
            else
                "$dollar{window.location.protocol}//$dollar{window.location.host}/api/"

        generateFile(
            "${project.group}.${project.name}.api",
            "ApiBase.kt",
            """
import kotlinx.browser.window
object ApiBase{
    val url: String = "$apiUrl"
}
            """
        )
    }
}

kotlin {
    js(IR) {
        useCommonJs()
        browser {
            webpackTask {
                outputFileName = jsName
            }
            testTask {
                enabled = (project.version as Version).isSnapshot
                useKarma {
                    useFirefoxHeadless()
                }
            }
        }
        binaries.executable()
    }
    sourceSets {
        val jsMain by getting {
            kotlin.srcDirs(generateSources.outputs)
            dependencies {
                implementation(kotlin("stdlib"))
                implementation(project(":sharedCode"))
                implementation("org.jetbrains.kotlinx:kotlinx-html-js:${buildVersions.DependencyVersions.kotlinHtml}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${buildVersions.DependencyVersions.kotlinTimeVersion}")
                implementation("io.ktor:ktor-client-content-negotiation:${buildVersions.DependencyVersions.ktorVersion}")
                implementation("io.ktor:ktor-serialization-kotlinx-json:${buildVersions.DependencyVersions.ktorVersion}")
                implementation("io.ktor:ktor-client-core:${buildVersions.DependencyVersions.ktorVersion}")
                implementation("io.ktor:ktor-client-serialization:${buildVersions.DependencyVersions.ktorVersion}")
                implementation("io.ktor:ktor-client-auth:${buildVersions.DependencyVersions.ktorVersion}")
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
    }
    //    sourceSets["main"].kotlin.srcDirs("${project.projectDir}/src/generated/kotlin", "${project.projectDir}/src/main/kotlin")
}

//tasks.test {
//    useTestNG()
//}
//
//tasks.compileKotlin2Js {
//    kotlinOptions {
//        outputFile = "${sourceSets.main.get().output.resourcesDir}/${project.name}.js"
//        sourceMap = true
//    }
//}
//
//val unpackKotlinJsStdlib by tasks.registering {
//    group = "build"
//    description = "Unpack the Kotlin JavaScript standard library"
//    val outputDir = file("$buildDir/$name")
//    inputs.property("compileClasspath", configurations.compileClasspath.get())
//    outputs.dir(outputDir)
//    doLast {
//        val all = configurations.compileClasspath.get()
//                .map { zipTree(it).files }
//                .flatten()
//        copy {
//            includeEmptyDirs = false
//            from(all)
//            into(outputDir)
//            include("**/*.js")
//            include("**/*.js.map")
//            exclude("META-INF/**")
//        }
//    }
//}
//
//val assembleWeb by tasks.registering(Copy::class) {
//    group = "build"
//    description = "Assemble the web application"
//    includeEmptyDirs = false
//    from(unpackKotlinJsStdlib)
//    from(sourceSets.main.get().output) {
//        exclude("**/*.kjsm")
//    }
//    into("$buildDir/web")
//}
//
val deployLocalKotlin by tasks.creating(Copy::class) {
    group = "deploy"
    from("${project.projectDir}/src/")
    into(File(project(":client").buildDir, "js/src"))
    mustRunAfter(tasks.assemble)
}

val deployLocal by tasks.creating(Copy::class) {
    dependsOn(tasks.assemble, deployLocalKotlin)
    group = "deploy"
    //    from("$buildDir/js/packages/clientJs/kotlin-dce/") {
    //        include("*.js")
    //        include("*.js.map")
    //    }
    from("$buildDir/dist/js/productionExecutable")
    into(File(project(":client").buildDir, "js"))
}
//
//tasks.assemble {
//    dependsOn(assembleWeb)
//}
//
//tasks.jacocoTestReport {
//    reports {
//        xml.isEnabled = false
//        csv.isEnabled = false
//        html.destination = file("${buildDir}/reports/jacocoHtml")
//    }
//}
//
//tasks.check {
//    dependsOn(tasks.jacocoTestReport)
//}

fun generateFile(packageName: String, fileName: String, fileContent: String) {
    val outputDir: File = file("${project.projectDir}/src/generated/kotlin/${packageName.replace(".", "/")}")
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(
        Paths.get(outputDir.absolutePath, fileName), """package $packageName

$fileContent
""".toByteArray()
    )
}

val createNoCacheFile = tasks.register("createNoCacheFile") {
    val outputDir = File(buildDir, "dist/js/productionExecutable")
    val nocacheFile = Paths.get(outputDir.absolutePath, "nocache.js")
    outputs.file(nocacheFile)

    doLast {
        if (Files.notExists(Paths.get(outputDir.absolutePath))) {
            Files.createDirectories(Paths.get(outputDir.absolutePath))
        }

        Files.write(
            nocacheFile,
            "import(\"./$jsName\");".toByteArray()
        )
    }
}

tasks.assemble {
    dependsOn(createNoCacheFile)
}

tasks.getByName("compileKotlinJs") {
    dependsOn(generateSources)
}
//
//tasks.dokka {
//    outputFormat = "html"
//    outputDirectory = "$buildDir/javadoc"
//}
//
//val dokkaJar by tasks.creating(Jar::class) {
//    group = JavaBasePlugin.DOCUMENTATION_GROUP
//    description = "Assembles Kotlin docs with Dokka"
//    archiveClassifier.set("javadoc")
//    from(tasks.dokka)
//    destinationDirectory.set(file("$buildDir/output"))
//}
//
//val sourcesJar by tasks.registering(Jar::class) {
//    archiveClassifier.set("sources")
//    from(sourceSets["main"].allSource)
//    destinationDirectory.set(file("$buildDir/output"))
//}

publishing {
    publications {
        create<MavenPublication>("default") {
            from(components["kotlin"])
            //            artifact(dokkaJar)
        }
    }
    repositories {
        maven {
            url = uri("$buildDir/repository")
        }
    }
}

ossIndexAudit {
    isShowAll = false
    isAllConfigurations = false
    isPrintBanner = false
    outputFormat = org.sonatype.gradle.plugins.scan.ossindex.OutputFormat.DEPENDENCY_GRAPH
}