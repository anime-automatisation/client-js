package de.comhix.anime.clientJs.utils

import kotlinx.browser.window
import org.w3c.dom.url.URLSearchParams

/**
 * @author Benjamin Beeker
 */
fun getParameter(parameter: String): String? {
    return URLSearchParams(window.location.search).get(parameter)
}