package de.comhix.anime.clientJs.components

import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.js.onChangeFunction
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.url.URLSearchParams
import kotlin.reflect.KMutableProperty0

/**
 * @author Benjamin Beeker
 */
abstract class Component<Data : Any?> {

    open val title: String
        get() = this::class.simpleName!!
    open val path: String
        get() = this::class.simpleName!!.lowercase()

    protected lateinit var data: Data & Any

    protected abstract suspend fun prepareData(): Data
    protected abstract fun DIV.render()

    operator fun get(parameter: String): String? {
        return URLSearchParams(window.location.search).get(parameter)
    }

    suspend fun render(): HTMLDivElement {
        val data = prepareData()
        if (data != null) {
            this.data = data
        }

        return document.create.div("content") {
            this.render()
        }
    }

    fun navigate(page: String, vararg additionalParams: Pair<String, Any?>) {
        val params = URLSearchParams(window.location.search)
        additionalParams.forEach {
            if (it.second != null)
                params.set(it.first, it.second.toString())
            else
                params.delete(it.first)
        }
        params.set("page", page)
        window.location.search = params.toString()
    }
}

fun FORM.addInput(
    name: String,
    text: String,
    value: Any? = null,
    type: InputType? = null,
    disabled: Boolean = false,
    additionalConfiguration: INPUT.() -> Unit = {},
    valueReceiver: KMutableProperty0<String>
) = addInput(name, text, value, type, disabled, additionalConfiguration) {
    valueReceiver.set(it)
}

fun FORM.addInput(
    name: String,
    text: String,
    value: Any? = null,
    type: InputType? = null,
    disabled: Boolean = false,
    additionalConfiguration: INPUT.() -> Unit = {},
    valueReceiver: (String) -> Unit
) {
    label("label") {
        htmlFor = name
        text(text)
    }
    input {
        this.disabled = disabled
        this.name = name
        if (value != null) {
            this.value = value.toString()
        }
        if (type != null) {
            this.type = type
        }
        onChangeFunction = {
            valueReceiver((it.target as HTMLInputElement).value)
        }
        additionalConfiguration()
    }
    br {}
}