package de.comhix.anime.clientJs.navigation

import de.comhix.anime.clientJs.LoggedInUserProvider
import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.clientJs.components.animes.Animes
import de.comhix.anime.clientJs.components.animes.Episodes
import de.comhix.anime.clientJs.components.torrents.TorrentsMenu
import de.comhix.anime.clientJs.components.user.Login
import de.comhix.anime.sharedCode.data.Role
import de.comhix.anime.sharedCode.data.role
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.html.a
import kotlinx.html.dom.create
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.ul
import kotlinx.html.li
import org.w3c.dom.NodeList

/**
 * @author Benjamin Beeker
 */
object Navigation {
    suspend fun create(): NodeList {
        val user = LoggedInUserProvider.getUser()

        return document.create.ul {
            when (user.role) {
                Role.GUEST -> li(classes = "horizontal-list") {
                    a(classes = "page-link") {
                        text("Login")
                        href = "?page=${Login.path}"
                    }
                }

                Role.ADMIN -> {
                    li(classes = "horizontal-list") {
                        a(classes = "page-link") {
                            text("Animes")
                            href = "?page=${Animes.path}"
                        }
                    }
                    text(" ")
                    li(classes = "horizontal-list") {
                        a(classes = "page-link") {
                            text("Episodes")
                            href = "?page=${Episodes.path}"
                        }
                    }
                    text(" ")
                    li(classes = "horizontal-list") {
                        a(classes = "page-link") {
                            text("Torrents")
                            href = "?page=${TorrentsMenu.path}"
                        }
                    }
                    text(" ")
                    li(classes = "horizontal-list") {
                        a(classes = "page-link") {
                            text("Logout")
                            href = "?page=${Login.path}"
                            onClickFunction = {
                                MainScope().launch {
                                    Api.Users.logout()
                                }
                            }
                        }
                    }
                }

                else       -> li(classes = "horizontal-list") {
                    a(classes = "page-link") {
                        text("Logout")
                        href = "?page=${Login.path}"
                        onClickFunction = {
                            MainScope().launch {
                                Api.Users.logout()
                            }
                        }
                    }
                }
            }
        }.childNodes
    }
}
