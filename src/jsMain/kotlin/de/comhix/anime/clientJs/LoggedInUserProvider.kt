package de.comhix.anime.clientJs

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.sharedCode.data.User
import kotlinx.browser.localStorage
import org.w3c.dom.get

/**
 * @author Benjamin Beeker
 */
object LoggedInUserProvider {
    private var user: User? = null
    private var initialized: Boolean = false

    suspend fun getUser(): User? {
        if (!initialized) {
            user = localStorage["token"]?.let {
                try {
                    Api.Users.getUser()
                }
                catch (e: Exception) {
                    localStorage.removeItem("token")
                    null
                }
            }
            initialized = true
        }

        return user
    }
}