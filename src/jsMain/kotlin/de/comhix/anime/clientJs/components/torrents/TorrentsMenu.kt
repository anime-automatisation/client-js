package de.comhix.anime.clientJs.components.torrents

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.clientJs.components.Component
import de.comhix.anime.clientJs.components.addInput
import de.comhix.anime.clientJs.components.notification.toast.Toast
import de.comhix.anime.clientJs.components.notification.toast.ToastPresenter
import de.comhix.anime.sharedCode.data.Torrent
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.datetime.*
import kotlinx.datetime.TimeZone.Companion
import kotlinx.datetime.format.DateTimeFormat
import kotlinx.datetime.format.char
import kotlinx.html.*
import kotlinx.html.js.onSubmitFunction
import org.w3c.dom.HTMLInputElement

/**
 * @author Benjamin Beeker
 */
object TorrentsMenu : Component<List<Torrent>>() {
    private val Format: DateTimeFormat<LocalDateTime> = LocalDateTime.Format {
        date(
            LocalDate.Format {
                dayOfMonth()
                char('.')
                monthNumber()
                char('.')
                year()
            }
        )
        chars(" - ")
        time(
            LocalTime.Format {
                hour(); char(':'); minute()
            }
        )
    }

    override val title: String = "Torrents"
    override suspend fun prepareData() = Api.Torrents.getTorrents()

    override fun DIV.render() {
        form {
            addInput("link", "Torrent- or Magnetlink", additionalConfiguration = {
                id = "link-input"
            }) {}
            submitInput { }

            onSubmitFunction = {
                it.preventDefault()
                val linkElement = (document.getElementById("link-input") as HTMLInputElement)

                linkElement.value.ifEmpty { null }?.let {
                    MainScope().launch {
                        Api.Torrents.addTorrent(it)
                        ToastPresenter.addToast(Toast("Torrent Added"))
                        linkElement.value = ""
                    }
                }
            }
        }
        window.navigator.clipboard.addEventListener("copy", {
            clipboardMonitoring()
        })
        clipboardMonitoring()

        br {}

        table {
            tr {
                th {
                    text("Name")
                }
                th(classes = "added") {
                    text("Added")
                }
                th(classes = "progress") {
                    text("Progress")
                }
            }
            data.sortedBy { -it.addedDate }
                .forEach {
                    tr {
                        td {
                            text(it.name)
                        }
                        td {
                            val formatted = Instant.fromEpochSeconds(it.addedDate).toLocalDateTime(Companion.UTC).format(Format)

                            text(formatted)
                        }
                        td {
                            val progress = (it.progress * 100).toInt()

                            div("progress-bar") {
                                style = "width: $progress%;"
                                text(progress)
                            }
                        }
                    }
                }
        }
    }

    private fun clipboardMonitoring() {
        window.navigator.clipboard.readText().then {
            if (it.startsWith("magnet:") || it.endsWith("torrent")) {
                (document.getElementById("link-input") as? HTMLInputElement)?.value = it
            }
        }
    }
}