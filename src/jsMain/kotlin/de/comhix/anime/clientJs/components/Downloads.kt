package de.comhix.anime.clientJs.components

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.sharedCode.data.File
import de.comhix.anime.sharedCode.util.file.FileSizeUtil.format
import kotlinx.browser.document
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.onClickFunction
import kotlin.js.Date

/**
 * @author Benjamin Beeker
 */
object Downloads : Component<List<File>>() {

    private const val listContainerId = "listContainer"
    override suspend fun prepareData(): List<File> {
        return Api.Files.list().sortedBy { -it.timestamp }
    }

    override fun DIV.render() {
        div {
            style = "font-weight:bold"
            text("Sort: ")
            a {
                text("Filename")
                onClickFunction = { event ->
                    event.preventDefault()
                    data = data.sortedBy { it.filename.lowercase() }
                    reRender()
                }
            }
            text(" | ")
            a {
                text("Date")
                onClickFunction = { event ->
                    event.preventDefault()
                    data = data.sortedBy { -it.timestamp }
                    reRender()
                }
            }
        }

        div {
            id = listContainerId

            renderList()
        }
    }

    private fun reRender() {
        val newContent = document.create.div { renderList() }.innerHTML

        document.getElementById(listContainerId)!!.innerHTML = newContent
    }

    private fun DIV.renderList() {
        table {
            data.forEach {
                tr {
                    td {
                        a {
                            //                            href = "${ApiBase.url}/file/${it.url}"
                            href = "/files/${it.filename}"
                            target = ATarget.blank
                            text(it.filename)
                        }
                    }
                    td {
                        text(Date(it.timestamp * 1000).toLocaleDateString())
                    }
                    td {
                        text(it.size.format())
                    }
                }
            }
        }
    }
}