package de.comhix.anime.clientJs.components.animes

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.clientJs.components.Component
import de.comhix.anime.sharedCode.data.Episode
import kotlinx.html.*
import kotlin.js.Date

/**
 * @author Benjamin Beeker
 */
object Episodes : Component<List<Episode>>() {
    override suspend fun prepareData(): List<Episode> {
        return Api.Episodes.list()
    }

    override fun DIV.render() {
        table {
            data.forEach { item ->
                val color =
                    when {
                        item.anime.selectedGroup != null -> "black"
                        item.filename.contains(" 01 ")   -> "darkseagreen"
                        else                             -> "darkgrey"
                    }

                tr {
                    style = "color:$color"
                    td {
                        text("${item.filename} ")
                        a {
                            target = ATarget.blank
                            href = item.link
                            i(classes = "fa-solid fa-magnet") {
                                style = "color:$color"
                            }
                        }
                        text(" ")
                        a {
                            target = ATarget.blank
                            href = "https://anidb.net/anime/?adb.search=${item.anime.name}&do.search=1"
                            i(classes = "fa-solid fa-magnifying-glass") {
                                style = "color:$color"
                            }
                        }
                    }
                    td {
                        text(Date(item.timestamp).toLocaleString())
                    }
                }
            }
        }
    }
}