@file:Suppress("NOTHING_TO_INLINE")

package de.comhix.anime.clientJs.utils

import de.comhix.anime.clientJs.utils.Level.*
import kotlin.reflect.KClass

/**
 * @author Benjamin Beeker
 */
@Deprecated("use supplier implementation", replaceWith = ReplaceWith("info{message}"))
inline fun Any.info(message: String) = info { message }
inline fun Any.info(noinline supplier: () -> Any?) = logger().log(INFO, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("info(throwable){message}"))
inline fun Any.info(message: String, throwable: Throwable) = info(throwable) { message }
inline fun Any.info(throwable: Throwable, noinline supplier: () -> Any?) = logger().log(INFO, throwable, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("debug{message}"))
inline fun Any.debug(message: String) = debug { message }
inline fun Any.debug(noinline supplier: () -> Any?) = logger().log(DEBUG, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("debug(throwable){message}"))
inline fun Any.debug(message: String, throwable: Throwable) = debug(throwable) { message }
inline fun Any.debug(throwable: Throwable, noinline supplier: () -> Any?) = logger().log(DEBUG, throwable, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("warn{message}"))
inline fun Any.warn(message: String) = warn { message }
inline fun Any.warn(noinline supplier: () -> Any?) = logger().log(WARNING, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("warn(throwable){message}"))
inline fun Any.warn(message: String, throwable: Throwable) = warn(throwable) { message }
inline fun Any.warn(throwable: Throwable, noinline supplier: () -> Any?) = logger().log(WARNING, throwable, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("error{message}"))
inline fun Any.error(message: String) = error { message }
inline fun Any.error(noinline supplier: () -> Any?) = logger().log(ERROR, supplier)

@Deprecated("use supplier implementation", replaceWith = ReplaceWith("error(throwable){message}"))
inline fun Any.error(message: String, throwable: Throwable) = error(throwable) { message }
inline fun Any.error(throwable: Throwable, noinline supplier: () -> Any?) = logger().log(ERROR, throwable, supplier)

fun Any.logger() = logger(this::class)

fun logger(klass: KClass<out Any>) = Logger(klass)

class Logger(klass: KClass<out Any>) {
    private val klassPrefix = "${klass.simpleName}:"
    fun log(level: Level, supplier: () -> Any?) {
        when (level) {
            DEBUG   -> console.log(klassPrefix, supplier())
            INFO    -> console.info(klassPrefix, supplier())
            WARNING -> console.warn(klassPrefix, supplier())
            ERROR   -> console.error(klassPrefix, supplier())
        }
    }

    fun log(level: Level, throwable: Throwable, supplier: () -> Any?) {
        when (level) {
            DEBUG   -> console.log(klassPrefix, supplier(), throwable)
            INFO    -> console.info(klassPrefix, supplier(), throwable)
            WARNING -> console.warn(klassPrefix, supplier(), throwable)
            ERROR   -> console.error(klassPrefix, supplier(), throwable)
        }
    }
}

enum class Level {
    DEBUG,
    INFO,
    WARNING,
    ERROR
}