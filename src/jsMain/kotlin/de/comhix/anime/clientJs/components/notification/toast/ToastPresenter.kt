package de.comhix.anime.clientJs.components.notification.toast

import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.dom.create
import org.w3c.dom.HTMLDivElement

/**
 * @author Benjamin Beeker
 */
object ToastPresenter {
    private val ToastContainerId = "toast-container"
    private val toastContainer: HTMLDivElement by lazy {
        document.getElementById(ToastContainerId) as? HTMLDivElement ?: run {
            document.body!!.appendChild(document.create.div("toast-container") { }) as HTMLDivElement
        }
    }
    private var toastId = 0

    fun addToast(toast: Toast) {
        toastContainer.append {
            val typeClass = "toast-${toast.type.name.lowercase()}"
            val element = div("toast $typeClass") {
                id = "toast-${toastId++}"
                span("toast-title $typeClass") {
                    text(toast.title)
                }
                span("toast-text $typeClass") {
                    text(toast.text)
                }
            }
            window.setTimeout(
                {
                    element.remove()
                },
                5000
            )
        }
    }
}