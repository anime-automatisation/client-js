package de.comhix.anime.clientJs.components.animes

import de.comhix.anime.clientJs.api.Api
import de.comhix.anime.clientJs.components.Component
import de.comhix.anime.clientJs.components.notification.toast.Toast
import de.comhix.anime.clientJs.components.notification.toast.ToastPresenter
import de.comhix.anime.clientJs.utils.info
import de.comhix.anime.sharedCode.data.Anime
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.html.*
import kotlinx.html.js.onInputFunction
import org.w3c.dom.HTMLSelectElement

/**
 * @author Benjamin Beeker
 */
object Animes : Component<List<Anime>>() {
    override suspend fun prepareData(): List<Anime> {
        return Api.Animes.list()
            .sortedBy { it.name }
            .sortedBy { !it.currentSeason }
    }

    override fun DIV.render() {
        form {
            style = "display:table"
            data.forEach { anime ->
                div("table-line") {
                    style = "display:table-row"
                    span {
                        style = "display:table-cell"
                        text("${anime.name} ")
                        a {
                            target = ATarget.blank
                            href = "https://anidb.net/anime/?adb.search=${anime.name}&do.search=1"
                            i(classes = "fa-solid fa-magnifying-glass")
                        }
                    }
                    select {
                        id = anime.name
                        style = "display:table-cell"
                        option {
                            value = ""
                            text("---")
                        }
                        anime.groups.forEach { group ->
                            option {
                                value = group
                                selected = group == anime.selectedGroup
                                text(group)
                            }
                        }

                        onInputFunction = { event ->
                            val select = event.target as HTMLSelectElement
                            val group = select.value.ifEmpty { null }
                            val update = anime.copy(selectedGroup = group)
                            MainScope().launch {
                                info { "update anime: $update" }
                                Api.Animes.update(update)
                                ToastPresenter.addToast(Toast("Updated ${anime.name}"))
                            }
                        }
                    }
                }
            }
        }
    }
}